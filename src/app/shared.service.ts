import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'


@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http:HttpClient) { }


  getPeopleList():Observable<any[]>{
        //return this.http.get<any[]>('https://ghibliapi.herokuapp.com/people')
        return this.http.get<any[]>('https://jsonplaceholder.typicode.com/users')
        
    }

  getPerson(id:any){      
      return this.http.get<any[]>('https://jsonplaceholder.typicode.com/users?id='+id)      
  }


  addPerson(val:any){
      return this.http.post('http://127.0.0.1/people/',val)      
  }
}
