import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PeopleComponent } from './people/people.component';

import {SharedService} from './shared.service'

import {HttpClientModule} from '@angular/common/http'
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { PeopleListComponent } from './people/people-list/people-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PeopleAddEditComponent } from './people/people-add-edit/people-add-edit.component'

@NgModule({
  declarations: [
    AppComponent,
    PeopleComponent,
    PeopleListComponent,
    PeopleAddEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
