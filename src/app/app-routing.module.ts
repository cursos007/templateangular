import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeopleComponent } from './people/people.component';
import { PeopleListComponent } from './people/people-list/people-list.component';
import { PeopleAddEditComponent } from './people/people-add-edit/people-add-edit.component';

const routes: Routes = [
  {path: 'people', component:PeopleComponent},
  {path: 'peoplelist', component:PeopleListComponent},
  {path: 'peopleadd/:id',component:PeopleAddEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const APP_ROUTES = RouterModule.forRoot( routes, { useHash: true });
