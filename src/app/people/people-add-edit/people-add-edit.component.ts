import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-people-add-edit',
  templateUrl: './people-add-edit.component.html',
  styleUrls: ['./people-add-edit.component.css']
})
export class PeopleAddEditComponent implements OnInit {
  public formPersona: UntypedFormGroup;

  constructor(private formBuilder: UntypedFormBuilder, private service: SharedService, private route: ActivatedRoute, private router: Router) {
    this.formPersona = this.formBuilder.group({
    });
  }

  ngOnInit(): void {   
    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'; 
    this.obtenerPersona(this.route.snapshot.paramMap.get('id'));
    this.formPersona = this.formBuilder.group({
      Id: ['', [Validators.required]],
      Name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(60)]],
      UserName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      Email: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      Phone: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      WebSite:['', [Validators.required, Validators.minLength(3), Validators.maxLength(256)]],
    })
  }

  obtenerPersona(id:string | null){
    this.service.getPerson(id).subscribe(data=>{
      console.log('Se solicitato el listado de personas');      
      let res = JSON.stringify(data);
      let obj = JSON.parse(res);
      obj=obj[0]
      this.formPersona.setValue({
        Id: obj.id,
        Name: obj.name,
        UserName:obj.username,
        Email:obj.email,
        Phone:obj.phone,
        WebSite:obj.website
      })
    });
  }




  send(): any {

    this.service.addPerson(this.formPersona.value).subscribe(data => {
      console.log('Se modifico la aplicación');
      console.log(this.formPersona.value)
      this.router.navigateByUrl('/peoplelist');
    });

  }
}
